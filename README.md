# Automotive Comprehensive Performance Workflow

Runs various workloads and benchmarks while collecing system metrics time-series data
with PCP.

## Requirements

- [Arcaflow engine](https://github.com/arcalot/arcaflow-engine/releases)
- Local container runtime, Podman Remote
  - The remote Podman service `connection` name is `ride4` as defined in `config.yaml`.
- Environment variables
  - `HORREUM_API_KEY` - Optional: The Horreum API key for uploading data to the Horreum service.

## Workflow Description

The primary [`workflow.yaml`](workflow.yaml) is designed to generate run-relevant
metadata, collect system metadata with Ansible [gather
facts](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/gather_facts_module.html)
using the Arcaflow [metadata
plugin](https://github.com/arcalot/arcaflow-plugin-metadata), and then to run loops of
sub-workflows with various parameters.

In addition to the workload, the sub-workflows collect system metrics with [Performance
Co-pilot](https://pcp.io/) using the Arcaflow [pcp
plugin](https://github.com/arcalot/arcaflow-plugin-pcp).

## Files

- [`workflow.yaml`](workflow.yaml) -- Defines the outer workflow input schema, the
  plugins and sub-workflow to run and their data relationships, and the output to
  present to the user
- [`workflow-stressng.yaml`](workflow-stressng.yaml) -- Defines the inner workflow input
  schema, plugins, and output for stress-ng tests. This workflow is looped over by the
  `workflow.yaml`, but it can also be used stand-alone.
- [`workflow-fio.yaml`](workflow-fio.yaml) -- Defines the inner workflow input
  schema, plugins, and output for fio tests. This workflow is looped over by the
  `workflow.yaml`, but it can also be used stand-alone.
- [`workflow-coremark-pro.yaml`](workflow-coremark-pro.yaml) -- Defines the inner
  workflow input schema, plugins, and output for CoreMark-Pro tests. This workflow is
  looped over by the `workflow.yaml`, but it can also be used stand-alone.
- [`workflow-autobench.yaml`](workflow-autobench.yaml) -- Defines the inner workflow
  input schema, plugins, and output for Autobench tests. This workflow is looped over by
  the `workflow.yaml`, but it can also be used stand-alone.
- [`workflow-sysbench-cpu.yaml`](workflow-sysbench-cpu.yaml) -- Defines the inner
  workflow input schema, plugins, and output for sysbench cpu tests. This workflow is
  looped over by the `workflow.yaml`, but it can also be used stand-alone.
- [`workflow-sysbench-memory.yaml`](workflow-sysbench-memory.yaml) -- Defines the inner
  workflow input schema, plugins, and output for sysbench memory tests. This workflow is
  looped over by the `workflow.yaml`, but it can also be used stand-alone.
- [`example-input-quick.yaml`](example-input-quick.yaml) -- Example input parameters
  that the user provides for running the outer workflow with a quick test.
- [`example-input-thorough.yaml`](example-input-thorough.yaml) -- Example input
  parameters that the user provides for running the outer workflow with a thorough test.
- [`example-input-stressng-cpu.yaml`](example-input-stressng-cpu.yaml) -- Example CPU
  test input parameters that can be used with the inner `workflow-stressng.yaml`
  workflow stand-alone.
- [`example-input-stressng-memory.yaml`](example-input-stressng-memory.yaml) -- Example
  memory test input parameters that can be used with the inner `workflow-stressng.yaml`
  workflow stand-alone.
- [`example-input-fio.yaml`](example-input-fio.yaml) -- Example
  storage test input parameters that can be used with the inner `workflow-fio.yaml`
  workflow stand-alone.
- [`example-input-coremark-pro_or_autobench.yaml`](example-input-coremark-pro_or_autobench.yaml)
  -- Example test input parameters that can be used with either of the inner
  `workflow-coremark-pro.yaml` or `workflow-autobench.yaml` workflows stand-alone.
- [`example-input-sysbench-cpu.yaml`](example-input-sysbench-cpu.yaml) -- Example
  test input parameters that can be used with the inner `workflow-sysbench-cpu.yaml`
  workflow stand-alone.
- [`example-input-sysbench-memory.yaml`](example-input-sysbench-memory.yaml) -- Example
  test input parameters that can be used with the inner `workflow-sysbench-memory.yaml`
  workflow stand-alone.
- [`config.yaml`](config.yaml) -- Global config parameters that are passed to the
  Arcaflow engine.

## Running the Workflow

Download the Arcaflow engine from: https://github.com/arcalot/arcaflow-engine/releases

Run the workflow (The `--config` is optional. The config file provided in this repo
enables debug log output.):
```bash
arcaflow --input example-input-quick.yaml [--config config.yaml]
```

Run a sub-workflow as a stand-alone workflow:
```bash
arcaflow --input example-input-stressng-cpu.yaml --workflow workflow-stressng.yaml
```

### Disabling Steps

The main workflow allows for steps to be disabled via the input file. By default, all
workload steps are enabled and the Horreum upload step is disabled. Each of these
accepts a `enable_<step_name>` parameter in the input file to control which steps you
want to run. *Note that when a step is disabled, its required input parameters are still
needed in the input file due to the way the input and workflow validation works.*

## Workflow Diagrams
These diagrams show the end-to-end workflow success-path logic.

### Parent Workflow
```mermaid
%% Mermaid markdown workflow
flowchart LR
%% Success path
input-->outputs.success
input-->steps.autobench_loop.enabling
input-->steps.autobench_loop.execute
input-->steps.coremark_loop.enabling
input-->steps.coremark_loop.execute
input-->steps.fio_loop.enabling
input-->steps.fio_loop.execute
input-->steps.horreum_upload.enabling
input-->steps.horreum_upload.starting
input-->steps.stressng_loop.enabling
input-->steps.stressng_loop.execute
input-->steps.sysbench_cpu_loop.enabling
input-->steps.sysbench_cpu_loop.execute
input-->steps.sysbench_memory_loop.enabling
input-->steps.sysbench_memory_loop.execute
outputs.success.horreum_disabled-->outputs.success
outputs.success.horreum_error-->outputs.success
outputs.success.horreum_run_id-->outputs.success
outputs.success.payload.autobench_workload-->outputs.success
outputs.success.payload.autobench_workload.disabled-->outputs.success.payload.autobench_workload
outputs.success.payload.autobench_workload.enabled-->outputs.success.payload.autobench_workload
outputs.success.payload.coremark_pro_workload-->outputs.success
outputs.success.payload.coremark_pro_workload.disabled-->outputs.success.payload.coremark_pro_workload
outputs.success.payload.coremark_pro_workload.enabled-->outputs.success.payload.coremark_pro_workload
outputs.success.payload.fio_workload-->outputs.success
outputs.success.payload.fio_workload.disabled-->outputs.success.payload.fio_workload
outputs.success.payload.fio_workload.enabled-->outputs.success.payload.fio_workload
outputs.success.payload.rhivos_config-->outputs.success
outputs.success.payload.stressng_workload-->outputs.success
outputs.success.payload.stressng_workload.disabled-->outputs.success.payload.stressng_workload
outputs.success.payload.stressng_workload.enabled-->outputs.success.payload.stressng_workload
outputs.success.payload.sysbench_cpu_workload-->outputs.success
outputs.success.payload.sysbench_cpu_workload.disabled-->outputs.success.payload.sysbench_cpu_workload
outputs.success.payload.sysbench_cpu_workload.enabled-->outputs.success.payload.sysbench_cpu_workload
outputs.success.payload.sysbench_memory_workload-->outputs.success
outputs.success.payload.sysbench_memory_workload.disabled-->outputs.success.payload.sysbench_memory_workload
outputs.success.payload.sysbench_memory_workload.enabled-->outputs.success.payload.sysbench_memory_workload
steps.ansible_facts.cancelled-->steps.ansible_facts.closed
steps.ansible_facts.cancelled-->steps.ansible_facts.outputs
steps.ansible_facts.closed-->steps.ansible_facts.closed.result
steps.ansible_facts.deploy-->steps.ansible_facts.closed
steps.ansible_facts.deploy-->steps.ansible_facts.starting
steps.ansible_facts.disabled-->steps.ansible_facts.disabled.output
steps.ansible_facts.enabling-->steps.ansible_facts.closed
steps.ansible_facts.enabling-->steps.ansible_facts.disabled
steps.ansible_facts.enabling-->steps.ansible_facts.enabling.resolved
steps.ansible_facts.enabling-->steps.ansible_facts.starting
steps.ansible_facts.outputs-->steps.ansible_facts.outputs.success
steps.ansible_facts.outputs.success-->outputs.success
steps.ansible_facts.outputs.success-->steps.horreum_upload.starting
steps.ansible_facts.running-->steps.ansible_facts.closed
steps.ansible_facts.running-->steps.ansible_facts.outputs
steps.ansible_facts.starting-->steps.ansible_facts.closed
steps.ansible_facts.starting-->steps.ansible_facts.running
steps.ansible_facts.starting-->steps.ansible_facts.starting.started
steps.autobench_loop.closed-->steps.autobench_loop.closed.result
steps.autobench_loop.disabled-->steps.autobench_loop.disabled.output
steps.autobench_loop.disabled.output-->outputs.success.payload.autobench_workload.disabled
steps.autobench_loop.disabled.output-->steps.horreum_upload.starting.data_object.autobench_workload.disabled
steps.autobench_loop.enabling-->steps.autobench_loop.closed
steps.autobench_loop.enabling-->steps.autobench_loop.disabled
steps.autobench_loop.enabling-->steps.autobench_loop.enabling.resolved
steps.autobench_loop.enabling-->steps.autobench_loop.execute
steps.autobench_loop.execute-->steps.autobench_loop.outputs
steps.autobench_loop.execute.-->steps.autobench_loop.execute
steps.autobench_loop.execute..disabled-->steps.autobench_loop.execute.
steps.autobench_loop.execute..enabled-->steps.autobench_loop.execute.
steps.autobench_loop.outputs-->steps.autobench_loop.outputs.success
steps.autobench_loop.outputs-->steps.end_timestamp.starting.f
steps.autobench_loop.outputs.success-->outputs.success.payload.autobench_workload.enabled
steps.autobench_loop.outputs.success-->steps.horreum_upload.starting.data_object.autobench_workload.enabled
steps.coremark_loop.closed-->steps.coremark_loop.closed.result
steps.coremark_loop.disabled-->steps.coremark_loop.disabled.output
steps.coremark_loop.disabled.output-->outputs.success.payload.coremark_pro_workload.disabled
steps.coremark_loop.disabled.output-->steps.autobench_loop.execute..disabled
steps.coremark_loop.disabled.output-->steps.horreum_upload.starting.data_object.coremark_pro_workload.disabled
steps.coremark_loop.enabling-->steps.coremark_loop.closed
steps.coremark_loop.enabling-->steps.coremark_loop.disabled
steps.coremark_loop.enabling-->steps.coremark_loop.enabling.resolved
steps.coremark_loop.enabling-->steps.coremark_loop.execute
steps.coremark_loop.execute-->steps.coremark_loop.outputs
steps.coremark_loop.execute.-->steps.coremark_loop.execute
steps.coremark_loop.execute..disabled-->steps.coremark_loop.execute.
steps.coremark_loop.execute..enabled-->steps.coremark_loop.execute.
steps.coremark_loop.outputs-->steps.autobench_loop.execute..enabled
steps.coremark_loop.outputs-->steps.coremark_loop.outputs.success
steps.coremark_loop.outputs-->steps.end_timestamp.starting.e
steps.coremark_loop.outputs.success-->outputs.success.payload.coremark_pro_workload.enabled
steps.coremark_loop.outputs.success-->steps.horreum_upload.starting.data_object.coremark_pro_workload.enabled
steps.end_timestamp.cancelled-->steps.end_timestamp.closed
steps.end_timestamp.cancelled-->steps.end_timestamp.outputs
steps.end_timestamp.closed-->steps.end_timestamp.closed.result
steps.end_timestamp.deploy-->steps.end_timestamp.closed
steps.end_timestamp.deploy-->steps.end_timestamp.starting
steps.end_timestamp.disabled-->steps.end_timestamp.disabled.output
steps.end_timestamp.enabling-->steps.end_timestamp.closed
steps.end_timestamp.enabling-->steps.end_timestamp.disabled
steps.end_timestamp.enabling-->steps.end_timestamp.enabling.resolved
steps.end_timestamp.enabling-->steps.end_timestamp.starting
steps.end_timestamp.outputs-->steps.end_timestamp.outputs.success
steps.end_timestamp.outputs.success-->outputs.success
steps.end_timestamp.outputs.success-->steps.horreum_upload.starting
steps.end_timestamp.running-->steps.end_timestamp.closed
steps.end_timestamp.running-->steps.end_timestamp.outputs
steps.end_timestamp.starting-->steps.end_timestamp.closed
steps.end_timestamp.starting-->steps.end_timestamp.running
steps.end_timestamp.starting-->steps.end_timestamp.starting.started
steps.end_timestamp.starting.a-->steps.end_timestamp.starting
steps.end_timestamp.starting.b-->steps.end_timestamp.starting
steps.end_timestamp.starting.c-->steps.end_timestamp.starting
steps.end_timestamp.starting.d-->steps.end_timestamp.starting
steps.end_timestamp.starting.e-->steps.end_timestamp.starting
steps.end_timestamp.starting.f-->steps.end_timestamp.starting
steps.fio_loop.closed-->steps.fio_loop.closed.result
steps.fio_loop.disabled-->steps.fio_loop.disabled.output
steps.fio_loop.disabled.output-->outputs.success.payload.fio_workload.disabled
steps.fio_loop.disabled.output-->steps.coremark_loop.execute..disabled
steps.fio_loop.disabled.output-->steps.horreum_upload.starting.data_object.fio_workload.disabled
steps.fio_loop.enabling-->steps.fio_loop.closed
steps.fio_loop.enabling-->steps.fio_loop.disabled
steps.fio_loop.enabling-->steps.fio_loop.enabling.resolved
steps.fio_loop.enabling-->steps.fio_loop.execute
steps.fio_loop.execute-->steps.fio_loop.outputs
steps.fio_loop.execute.-->steps.fio_loop.execute
steps.fio_loop.execute..disabled-->steps.fio_loop.execute.
steps.fio_loop.execute..enabled-->steps.fio_loop.execute.
steps.fio_loop.outputs-->steps.coremark_loop.execute..enabled
steps.fio_loop.outputs-->steps.end_timestamp.starting.d
steps.fio_loop.outputs-->steps.fio_loop.outputs.success
steps.fio_loop.outputs.success-->outputs.success.payload.fio_workload.enabled
steps.fio_loop.outputs.success-->steps.horreum_upload.starting.data_object.fio_workload.enabled
steps.horreum_upload.cancelled-->steps.horreum_upload.closed
steps.horreum_upload.cancelled-->steps.horreum_upload.outputs
steps.horreum_upload.closed-->steps.horreum_upload.closed.result
steps.horreum_upload.deploy-->steps.horreum_upload.closed
steps.horreum_upload.deploy-->steps.horreum_upload.starting
steps.horreum_upload.disabled-->steps.horreum_upload.disabled.output
steps.horreum_upload.disabled.output-->outputs.success.horreum_disabled
steps.horreum_upload.enabling-->steps.horreum_upload.closed
steps.horreum_upload.enabling-->steps.horreum_upload.disabled
steps.horreum_upload.enabling-->steps.horreum_upload.enabling.resolved
steps.horreum_upload.enabling-->steps.horreum_upload.starting
steps.horreum_upload.outputs-->steps.horreum_upload.outputs.success
steps.horreum_upload.outputs.error-->outputs.success.horreum_error
steps.horreum_upload.outputs.success-->outputs.success.horreum_run_id
steps.horreum_upload.running-->steps.horreum_upload.closed
steps.horreum_upload.running-->steps.horreum_upload.outputs
steps.horreum_upload.starting-->steps.horreum_upload.closed
steps.horreum_upload.starting-->steps.horreum_upload.running
steps.horreum_upload.starting-->steps.horreum_upload.starting.started
steps.horreum_upload.starting.data_object.autobench_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.autobench_workload.disabled-->steps.horreum_upload.starting.data_object.autobench_workload
steps.horreum_upload.starting.data_object.autobench_workload.enabled-->steps.horreum_upload.starting.data_object.autobench_workload
steps.horreum_upload.starting.data_object.coremark_pro_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.coremark_pro_workload.disabled-->steps.horreum_upload.starting.data_object.coremark_pro_workload
steps.horreum_upload.starting.data_object.coremark_pro_workload.enabled-->steps.horreum_upload.starting.data_object.coremark_pro_workload
steps.horreum_upload.starting.data_object.fio_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.fio_workload.disabled-->steps.horreum_upload.starting.data_object.fio_workload
steps.horreum_upload.starting.data_object.fio_workload.enabled-->steps.horreum_upload.starting.data_object.fio_workload
steps.horreum_upload.starting.data_object.rhivos_config-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.stressng_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.stressng_workload.disabled-->steps.horreum_upload.starting.data_object.stressng_workload
steps.horreum_upload.starting.data_object.stressng_workload.enabled-->steps.horreum_upload.starting.data_object.stressng_workload
steps.horreum_upload.starting.data_object.sysbench_cpu_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.sysbench_cpu_workload.disabled-->steps.horreum_upload.starting.data_object.sysbench_cpu_workload
steps.horreum_upload.starting.data_object.sysbench_cpu_workload.enabled-->steps.horreum_upload.starting.data_object.sysbench_cpu_workload
steps.horreum_upload.starting.data_object.sysbench_memory_workload-->steps.horreum_upload.starting
steps.horreum_upload.starting.data_object.sysbench_memory_workload.disabled-->steps.horreum_upload.starting.data_object.sysbench_memory_workload
steps.horreum_upload.starting.data_object.sysbench_memory_workload.enabled-->steps.horreum_upload.starting.data_object.sysbench_memory_workload
steps.rhivos_config.cancelled-->steps.rhivos_config.closed
steps.rhivos_config.cancelled-->steps.rhivos_config.outputs
steps.rhivos_config.closed-->steps.rhivos_config.closed.result
steps.rhivos_config.deploy-->steps.rhivos_config.closed
steps.rhivos_config.deploy-->steps.rhivos_config.starting
steps.rhivos_config.disabled-->steps.rhivos_config.disabled.output
steps.rhivos_config.enabling-->steps.rhivos_config.closed
steps.rhivos_config.enabling-->steps.rhivos_config.disabled
steps.rhivos_config.enabling-->steps.rhivos_config.enabling.resolved
steps.rhivos_config.enabling-->steps.rhivos_config.starting
steps.rhivos_config.outputs-->steps.rhivos_config.outputs.success
steps.rhivos_config.outputs.success-->outputs.success.payload.rhivos_config
steps.rhivos_config.outputs.success-->steps.horreum_upload.starting.data_object.rhivos_config
steps.rhivos_config.running-->steps.rhivos_config.closed
steps.rhivos_config.running-->steps.rhivos_config.outputs
steps.rhivos_config.starting-->steps.rhivos_config.closed
steps.rhivos_config.starting-->steps.rhivos_config.running
steps.rhivos_config.starting-->steps.rhivos_config.starting.started
steps.start_timestamp.cancelled-->steps.start_timestamp.closed
steps.start_timestamp.cancelled-->steps.start_timestamp.outputs
steps.start_timestamp.closed-->steps.start_timestamp.closed.result
steps.start_timestamp.deploy-->steps.start_timestamp.closed
steps.start_timestamp.deploy-->steps.start_timestamp.starting
steps.start_timestamp.disabled-->steps.start_timestamp.disabled.output
steps.start_timestamp.enabling-->steps.start_timestamp.closed
steps.start_timestamp.enabling-->steps.start_timestamp.disabled
steps.start_timestamp.enabling-->steps.start_timestamp.enabling.resolved
steps.start_timestamp.enabling-->steps.start_timestamp.starting
steps.start_timestamp.outputs-->steps.start_timestamp.outputs.success
steps.start_timestamp.outputs-->steps.stressng_loop.execute
steps.start_timestamp.outputs.success-->outputs.success
steps.start_timestamp.outputs.success-->steps.horreum_upload.starting
steps.start_timestamp.running-->steps.start_timestamp.closed
steps.start_timestamp.running-->steps.start_timestamp.outputs
steps.start_timestamp.starting-->steps.start_timestamp.closed
steps.start_timestamp.starting-->steps.start_timestamp.running
steps.start_timestamp.starting-->steps.start_timestamp.starting.started
steps.stressng_loop.closed-->steps.stressng_loop.closed.result
steps.stressng_loop.disabled-->steps.stressng_loop.disabled.output
steps.stressng_loop.disabled.output-->outputs.success.payload.stressng_workload.disabled
steps.stressng_loop.disabled.output-->steps.horreum_upload.starting.data_object.stressng_workload.disabled
steps.stressng_loop.disabled.output-->steps.sysbench_cpu_loop.execute..disabled
steps.stressng_loop.enabling-->steps.stressng_loop.closed
steps.stressng_loop.enabling-->steps.stressng_loop.disabled
steps.stressng_loop.enabling-->steps.stressng_loop.enabling.resolved
steps.stressng_loop.enabling-->steps.stressng_loop.execute
steps.stressng_loop.execute-->steps.stressng_loop.outputs
steps.stressng_loop.outputs-->steps.end_timestamp.starting.a
steps.stressng_loop.outputs-->steps.stressng_loop.outputs.success
steps.stressng_loop.outputs-->steps.sysbench_cpu_loop.execute..enabled
steps.stressng_loop.outputs.success-->outputs.success.payload.stressng_workload.enabled
steps.stressng_loop.outputs.success-->steps.horreum_upload.starting.data_object.stressng_workload.enabled
steps.sysbench_cpu_loop.closed-->steps.sysbench_cpu_loop.closed.result
steps.sysbench_cpu_loop.disabled-->steps.sysbench_cpu_loop.disabled.output
steps.sysbench_cpu_loop.disabled.output-->outputs.success.payload.sysbench_cpu_workload.disabled
steps.sysbench_cpu_loop.disabled.output-->steps.horreum_upload.starting.data_object.sysbench_cpu_workload.disabled
steps.sysbench_cpu_loop.disabled.output-->steps.sysbench_memory_loop.execute..disabled
steps.sysbench_cpu_loop.enabling-->steps.sysbench_cpu_loop.closed
steps.sysbench_cpu_loop.enabling-->steps.sysbench_cpu_loop.disabled
steps.sysbench_cpu_loop.enabling-->steps.sysbench_cpu_loop.enabling.resolved
steps.sysbench_cpu_loop.enabling-->steps.sysbench_cpu_loop.execute
steps.sysbench_cpu_loop.execute-->steps.sysbench_cpu_loop.outputs
steps.sysbench_cpu_loop.execute.-->steps.sysbench_cpu_loop.execute
steps.sysbench_cpu_loop.execute..disabled-->steps.sysbench_cpu_loop.execute.
steps.sysbench_cpu_loop.execute..enabled-->steps.sysbench_cpu_loop.execute.
steps.sysbench_cpu_loop.outputs-->steps.end_timestamp.starting.b
steps.sysbench_cpu_loop.outputs-->steps.sysbench_cpu_loop.outputs.success
steps.sysbench_cpu_loop.outputs-->steps.sysbench_memory_loop.execute..enabled
steps.sysbench_cpu_loop.outputs.success-->outputs.success.payload.sysbench_cpu_workload.enabled
steps.sysbench_cpu_loop.outputs.success-->steps.horreum_upload.starting.data_object.sysbench_cpu_workload.enabled
steps.sysbench_memory_loop.closed-->steps.sysbench_memory_loop.closed.result
steps.sysbench_memory_loop.disabled-->steps.sysbench_memory_loop.disabled.output
steps.sysbench_memory_loop.disabled.output-->outputs.success.payload.sysbench_memory_workload.disabled
steps.sysbench_memory_loop.disabled.output-->steps.fio_loop.execute..disabled
steps.sysbench_memory_loop.disabled.output-->steps.horreum_upload.starting.data_object.sysbench_memory_workload.disabled
steps.sysbench_memory_loop.enabling-->steps.sysbench_memory_loop.closed
steps.sysbench_memory_loop.enabling-->steps.sysbench_memory_loop.disabled
steps.sysbench_memory_loop.enabling-->steps.sysbench_memory_loop.enabling.resolved
steps.sysbench_memory_loop.enabling-->steps.sysbench_memory_loop.execute
steps.sysbench_memory_loop.execute-->steps.sysbench_memory_loop.outputs
steps.sysbench_memory_loop.execute.-->steps.sysbench_memory_loop.execute
steps.sysbench_memory_loop.execute..disabled-->steps.sysbench_memory_loop.execute.
steps.sysbench_memory_loop.execute..enabled-->steps.sysbench_memory_loop.execute.
steps.sysbench_memory_loop.outputs-->steps.end_timestamp.starting.c
steps.sysbench_memory_loop.outputs-->steps.fio_loop.execute..enabled
steps.sysbench_memory_loop.outputs-->steps.sysbench_memory_loop.outputs.success
steps.sysbench_memory_loop.outputs.success-->outputs.success.payload.sysbench_memory_workload.enabled
steps.sysbench_memory_loop.outputs.success-->steps.horreum_upload.starting.data_object.sysbench_memory_workload.enabled
steps.uuidgen.cancelled-->steps.uuidgen.closed
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.closed-->steps.uuidgen.closed.result
steps.uuidgen.deploy-->steps.uuidgen.closed
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.disabled-->steps.uuidgen.disabled.output
steps.uuidgen.enabling-->steps.uuidgen.closed
steps.uuidgen.enabling-->steps.uuidgen.disabled
steps.uuidgen.enabling-->steps.uuidgen.enabling.resolved
steps.uuidgen.enabling-->steps.uuidgen.starting
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.uuidgen.outputs.success-->steps.horreum_upload.starting
steps.uuidgen.running-->steps.uuidgen.closed
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.starting-->steps.uuidgen.closed
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.starting.started
```

### stress-ng Sub-Workflow
```mermaid
%% Mermaid markdown workflow
flowchart LR
%% Success path
input-->steps.pcp.starting
input-->steps.stressng.starting
steps.pcp.cancelled-->steps.pcp.closed
steps.pcp.cancelled-->steps.pcp.outputs
steps.pcp.closed-->steps.pcp.closed.result
steps.pcp.deploy-->steps.pcp.closed
steps.pcp.deploy-->steps.pcp.starting
steps.pcp.disabled-->steps.pcp.disabled.output
steps.pcp.enabling-->steps.pcp.closed
steps.pcp.enabling-->steps.pcp.disabled
steps.pcp.enabling-->steps.pcp.enabling.resolved
steps.pcp.enabling-->steps.pcp.starting
steps.pcp.outputs-->steps.pcp.outputs.success
steps.pcp.outputs.success-->outputs.success
steps.pcp.running-->steps.pcp.closed
steps.pcp.running-->steps.pcp.outputs
steps.pcp.starting-->steps.pcp.closed
steps.pcp.starting-->steps.pcp.running
steps.pcp.starting-->steps.pcp.starting.started
steps.pcp.starting.started-->steps.pre_wait.starting
steps.post_wait.cancelled-->steps.post_wait.closed
steps.post_wait.cancelled-->steps.post_wait.outputs
steps.post_wait.closed-->steps.post_wait.closed.result
steps.post_wait.deploy-->steps.post_wait.closed
steps.post_wait.deploy-->steps.post_wait.starting
steps.post_wait.disabled-->steps.post_wait.disabled.output
steps.post_wait.enabling-->steps.post_wait.closed
steps.post_wait.enabling-->steps.post_wait.disabled
steps.post_wait.enabling-->steps.post_wait.enabling.resolved
steps.post_wait.enabling-->steps.post_wait.starting
steps.post_wait.outputs-->steps.pcp.cancelled
steps.post_wait.outputs-->steps.post_wait.outputs.success
steps.post_wait.running-->steps.post_wait.closed
steps.post_wait.running-->steps.post_wait.outputs
steps.post_wait.starting-->steps.post_wait.closed
steps.post_wait.starting-->steps.post_wait.running
steps.post_wait.starting-->steps.post_wait.starting.started
steps.pre_wait.cancelled-->steps.pre_wait.closed
steps.pre_wait.cancelled-->steps.pre_wait.outputs
steps.pre_wait.closed-->steps.pre_wait.closed.result
steps.pre_wait.deploy-->steps.pre_wait.closed
steps.pre_wait.deploy-->steps.pre_wait.starting
steps.pre_wait.disabled-->steps.pre_wait.disabled.output
steps.pre_wait.enabling-->steps.pre_wait.closed
steps.pre_wait.enabling-->steps.pre_wait.disabled
steps.pre_wait.enabling-->steps.pre_wait.enabling.resolved
steps.pre_wait.enabling-->steps.pre_wait.starting
steps.pre_wait.outputs-->steps.pre_wait.outputs.success
steps.pre_wait.outputs-->steps.stressng.starting
steps.pre_wait.running-->steps.pre_wait.closed
steps.pre_wait.running-->steps.pre_wait.outputs
steps.pre_wait.starting-->steps.pre_wait.closed
steps.pre_wait.starting-->steps.pre_wait.running
steps.pre_wait.starting-->steps.pre_wait.starting.started
steps.stressng.cancelled-->steps.stressng.closed
steps.stressng.cancelled-->steps.stressng.outputs
steps.stressng.closed-->steps.stressng.closed.result
steps.stressng.deploy-->steps.stressng.closed
steps.stressng.deploy-->steps.stressng.starting
steps.stressng.disabled-->steps.stressng.disabled.output
steps.stressng.enabling-->steps.stressng.closed
steps.stressng.enabling-->steps.stressng.disabled
steps.stressng.enabling-->steps.stressng.enabling.resolved
steps.stressng.enabling-->steps.stressng.starting
steps.stressng.outputs-->steps.post_wait.starting
steps.stressng.outputs-->steps.stressng.outputs.success
steps.stressng.outputs-->steps.timerlat.cancelled
steps.stressng.outputs.success-->outputs.success
steps.stressng.running-->steps.stressng.closed
steps.stressng.running-->steps.stressng.outputs
steps.stressng.starting-->steps.stressng.closed
steps.stressng.starting-->steps.stressng.running
steps.stressng.starting-->steps.stressng.starting.started
steps.stressng.starting.started-->steps.timerlat.starting
steps.timerlat.cancelled-->steps.timerlat.closed
steps.timerlat.cancelled-->steps.timerlat.outputs
steps.timerlat.closed-->steps.timerlat.closed.result
steps.timerlat.deploy-->steps.timerlat.closed
steps.timerlat.deploy-->steps.timerlat.starting
steps.timerlat.disabled-->steps.timerlat.disabled.output
steps.timerlat.enabling-->steps.timerlat.closed
steps.timerlat.enabling-->steps.timerlat.disabled
steps.timerlat.enabling-->steps.timerlat.enabling.resolved
steps.timerlat.enabling-->steps.timerlat.starting
steps.timerlat.outputs-->steps.timerlat.outputs.success
steps.timerlat.outputs.success-->outputs.success
steps.timerlat.running-->steps.timerlat.closed
steps.timerlat.running-->steps.timerlat.outputs
steps.timerlat.starting-->steps.timerlat.closed
steps.timerlat.starting-->steps.timerlat.running
steps.timerlat.starting-->steps.timerlat.starting.started
steps.uuidgen.cancelled-->steps.uuidgen.closed
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.closed-->steps.uuidgen.closed.result
steps.uuidgen.deploy-->steps.uuidgen.closed
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.disabled-->steps.uuidgen.disabled.output
steps.uuidgen.enabling-->steps.uuidgen.closed
steps.uuidgen.enabling-->steps.uuidgen.disabled
steps.uuidgen.enabling-->steps.uuidgen.enabling.resolved
steps.uuidgen.enabling-->steps.uuidgen.starting
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.uuidgen.running-->steps.uuidgen.closed
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.starting-->steps.uuidgen.closed
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.starting.started
```

### Sysbench Sub-Workflow
```mermaid
%% Mermaid markdown workflow
flowchart LR
%% Success path
input-->steps.pcp.starting
input-->steps.sysbench.starting
steps.pcp.cancelled-->steps.pcp.closed
steps.pcp.cancelled-->steps.pcp.outputs
steps.pcp.closed-->steps.pcp.closed.result
steps.pcp.deploy-->steps.pcp.closed
steps.pcp.deploy-->steps.pcp.starting
steps.pcp.disabled-->steps.pcp.disabled.output
steps.pcp.enabling-->steps.pcp.closed
steps.pcp.enabling-->steps.pcp.disabled
steps.pcp.enabling-->steps.pcp.enabling.resolved
steps.pcp.enabling-->steps.pcp.starting
steps.pcp.outputs-->steps.pcp.outputs.success
steps.pcp.outputs.success-->outputs.success
steps.pcp.running-->steps.pcp.closed
steps.pcp.running-->steps.pcp.outputs
steps.pcp.starting-->steps.pcp.closed
steps.pcp.starting-->steps.pcp.running
steps.pcp.starting-->steps.pcp.starting.started
steps.pcp.starting.started-->steps.pre_wait.starting
steps.post_wait.cancelled-->steps.post_wait.closed
steps.post_wait.cancelled-->steps.post_wait.outputs
steps.post_wait.closed-->steps.post_wait.closed.result
steps.post_wait.deploy-->steps.post_wait.closed
steps.post_wait.deploy-->steps.post_wait.starting
steps.post_wait.disabled-->steps.post_wait.disabled.output
steps.post_wait.enabling-->steps.post_wait.closed
steps.post_wait.enabling-->steps.post_wait.disabled
steps.post_wait.enabling-->steps.post_wait.enabling.resolved
steps.post_wait.enabling-->steps.post_wait.starting
steps.post_wait.outputs-->steps.pcp.cancelled
steps.post_wait.outputs-->steps.post_wait.outputs.success
steps.post_wait.running-->steps.post_wait.closed
steps.post_wait.running-->steps.post_wait.outputs
steps.post_wait.starting-->steps.post_wait.closed
steps.post_wait.starting-->steps.post_wait.running
steps.post_wait.starting-->steps.post_wait.starting.started
steps.pre_wait.cancelled-->steps.pre_wait.closed
steps.pre_wait.cancelled-->steps.pre_wait.outputs
steps.pre_wait.closed-->steps.pre_wait.closed.result
steps.pre_wait.deploy-->steps.pre_wait.closed
steps.pre_wait.deploy-->steps.pre_wait.starting
steps.pre_wait.disabled-->steps.pre_wait.disabled.output
steps.pre_wait.enabling-->steps.pre_wait.closed
steps.pre_wait.enabling-->steps.pre_wait.disabled
steps.pre_wait.enabling-->steps.pre_wait.enabling.resolved
steps.pre_wait.enabling-->steps.pre_wait.starting
steps.pre_wait.outputs-->steps.pre_wait.outputs.success
steps.pre_wait.outputs-->steps.sysbench.starting
steps.pre_wait.running-->steps.pre_wait.closed
steps.pre_wait.running-->steps.pre_wait.outputs
steps.pre_wait.starting-->steps.pre_wait.closed
steps.pre_wait.starting-->steps.pre_wait.running
steps.pre_wait.starting-->steps.pre_wait.starting.started
steps.sysbench.cancelled-->steps.sysbench.closed
steps.sysbench.cancelled-->steps.sysbench.outputs
steps.sysbench.closed-->steps.sysbench.closed.result
steps.sysbench.deploy-->steps.sysbench.closed
steps.sysbench.deploy-->steps.sysbench.starting
steps.sysbench.disabled-->steps.sysbench.disabled.output
steps.sysbench.enabling-->steps.sysbench.closed
steps.sysbench.enabling-->steps.sysbench.disabled
steps.sysbench.enabling-->steps.sysbench.enabling.resolved
steps.sysbench.enabling-->steps.sysbench.starting
steps.sysbench.outputs-->steps.post_wait.starting
steps.sysbench.outputs-->steps.sysbench.outputs.success
steps.sysbench.outputs.success-->outputs.success
steps.sysbench.running-->steps.sysbench.closed
steps.sysbench.running-->steps.sysbench.outputs
steps.sysbench.starting-->steps.sysbench.closed
steps.sysbench.starting-->steps.sysbench.running
steps.sysbench.starting-->steps.sysbench.starting.started
steps.uuidgen.cancelled-->steps.uuidgen.closed
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.closed-->steps.uuidgen.closed.result
steps.uuidgen.deploy-->steps.uuidgen.closed
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.disabled-->steps.uuidgen.disabled.output
steps.uuidgen.enabling-->steps.uuidgen.closed
steps.uuidgen.enabling-->steps.uuidgen.disabled
steps.uuidgen.enabling-->steps.uuidgen.enabling.resolved
steps.uuidgen.enabling-->steps.uuidgen.starting
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.uuidgen.running-->steps.uuidgen.closed
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.starting-->steps.uuidgen.closed
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.starting.started
```

### Fio Sub-Workflow
```mermaid
%% Mermaid markdown workflow
flowchart LR
%% Success path
input-->steps.fio.starting
input-->steps.pcp.starting
steps.fio.cancelled-->steps.fio.closed
steps.fio.cancelled-->steps.fio.outputs
steps.fio.closed-->steps.fio.closed.result
steps.fio.deploy-->steps.fio.closed
steps.fio.deploy-->steps.fio.starting
steps.fio.disabled-->steps.fio.disabled.output
steps.fio.enabling-->steps.fio.closed
steps.fio.enabling-->steps.fio.disabled
steps.fio.enabling-->steps.fio.enabling.resolved
steps.fio.enabling-->steps.fio.starting
steps.fio.outputs-->steps.fio.outputs.success
steps.fio.outputs-->steps.post_wait.starting
steps.fio.outputs.success-->outputs.success
steps.fio.running-->steps.fio.closed
steps.fio.running-->steps.fio.outputs
steps.fio.starting-->steps.fio.closed
steps.fio.starting-->steps.fio.running
steps.fio.starting-->steps.fio.starting.started
steps.pcp.cancelled-->steps.pcp.closed
steps.pcp.cancelled-->steps.pcp.outputs
steps.pcp.closed-->steps.pcp.closed.result
steps.pcp.deploy-->steps.pcp.closed
steps.pcp.deploy-->steps.pcp.starting
steps.pcp.disabled-->steps.pcp.disabled.output
steps.pcp.enabling-->steps.pcp.closed
steps.pcp.enabling-->steps.pcp.disabled
steps.pcp.enabling-->steps.pcp.enabling.resolved
steps.pcp.enabling-->steps.pcp.starting
steps.pcp.outputs-->steps.pcp.outputs.success
steps.pcp.outputs.success-->outputs.success
steps.pcp.running-->steps.pcp.closed
steps.pcp.running-->steps.pcp.outputs
steps.pcp.starting-->steps.pcp.closed
steps.pcp.starting-->steps.pcp.running
steps.pcp.starting-->steps.pcp.starting.started
steps.pcp.starting.started-->steps.pre_wait.starting
steps.post_wait.cancelled-->steps.post_wait.closed
steps.post_wait.cancelled-->steps.post_wait.outputs
steps.post_wait.closed-->steps.post_wait.closed.result
steps.post_wait.deploy-->steps.post_wait.closed
steps.post_wait.deploy-->steps.post_wait.starting
steps.post_wait.disabled-->steps.post_wait.disabled.output
steps.post_wait.enabling-->steps.post_wait.closed
steps.post_wait.enabling-->steps.post_wait.disabled
steps.post_wait.enabling-->steps.post_wait.enabling.resolved
steps.post_wait.enabling-->steps.post_wait.starting
steps.post_wait.outputs-->steps.pcp.cancelled
steps.post_wait.outputs-->steps.post_wait.outputs.success
steps.post_wait.running-->steps.post_wait.closed
steps.post_wait.running-->steps.post_wait.outputs
steps.post_wait.starting-->steps.post_wait.closed
steps.post_wait.starting-->steps.post_wait.running
steps.post_wait.starting-->steps.post_wait.starting.started
steps.pre_wait.cancelled-->steps.pre_wait.closed
steps.pre_wait.cancelled-->steps.pre_wait.outputs
steps.pre_wait.closed-->steps.pre_wait.closed.result
steps.pre_wait.deploy-->steps.pre_wait.closed
steps.pre_wait.deploy-->steps.pre_wait.starting
steps.pre_wait.disabled-->steps.pre_wait.disabled.output
steps.pre_wait.enabling-->steps.pre_wait.closed
steps.pre_wait.enabling-->steps.pre_wait.disabled
steps.pre_wait.enabling-->steps.pre_wait.enabling.resolved
steps.pre_wait.enabling-->steps.pre_wait.starting
steps.pre_wait.outputs-->steps.fio.starting
steps.pre_wait.outputs-->steps.pre_wait.outputs.success
steps.pre_wait.running-->steps.pre_wait.closed
steps.pre_wait.running-->steps.pre_wait.outputs
steps.pre_wait.starting-->steps.pre_wait.closed
steps.pre_wait.starting-->steps.pre_wait.running
steps.pre_wait.starting-->steps.pre_wait.starting.started
steps.uuidgen.cancelled-->steps.uuidgen.closed
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.closed-->steps.uuidgen.closed.result
steps.uuidgen.deploy-->steps.uuidgen.closed
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.disabled-->steps.uuidgen.disabled.output
steps.uuidgen.enabling-->steps.uuidgen.closed
steps.uuidgen.enabling-->steps.uuidgen.disabled
steps.uuidgen.enabling-->steps.uuidgen.enabling.resolved
steps.uuidgen.enabling-->steps.uuidgen.starting
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.uuidgen.running-->steps.uuidgen.closed
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.starting-->steps.uuidgen.closed
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.starting.started
```

### CoreMark-Pro / Autobench Sub-Workflow
```mermaid
%% Mermaid markdown workflow
flowchart LR
%% Success path
input-->steps.pcp.starting
input-->steps.tune_iterations.starting
steps.certify_all.cancelled-->steps.certify_all.closed
steps.certify_all.cancelled-->steps.certify_all.outputs
steps.certify_all.closed-->steps.certify_all.closed.result
steps.certify_all.deploy-->steps.certify_all.closed
steps.certify_all.deploy-->steps.certify_all.starting
steps.certify_all.disabled-->steps.certify_all.disabled.output
steps.certify_all.enabling-->steps.certify_all.closed
steps.certify_all.enabling-->steps.certify_all.disabled
steps.certify_all.enabling-->steps.certify_all.enabling.resolved
steps.certify_all.enabling-->steps.certify_all.starting
steps.certify_all.outputs-->steps.certify_all.outputs.success
steps.certify_all.outputs-->steps.post_wait.starting
steps.certify_all.outputs.success-->outputs.success
steps.certify_all.running-->steps.certify_all.closed
steps.certify_all.running-->steps.certify_all.outputs
steps.certify_all.starting-->steps.certify_all.closed
steps.certify_all.starting-->steps.certify_all.running
steps.certify_all.starting-->steps.certify_all.starting.started
steps.pcp.cancelled-->steps.pcp.closed
steps.pcp.cancelled-->steps.pcp.outputs
steps.pcp.closed-->steps.pcp.closed.result
steps.pcp.deploy-->steps.pcp.closed
steps.pcp.deploy-->steps.pcp.starting
steps.pcp.disabled-->steps.pcp.disabled.output
steps.pcp.enabling-->steps.pcp.closed
steps.pcp.enabling-->steps.pcp.disabled
steps.pcp.enabling-->steps.pcp.enabling.resolved
steps.pcp.enabling-->steps.pcp.starting
steps.pcp.outputs-->steps.pcp.outputs.success
steps.pcp.outputs.success-->outputs.success
steps.pcp.running-->steps.pcp.closed
steps.pcp.running-->steps.pcp.outputs
steps.pcp.starting-->steps.pcp.closed
steps.pcp.starting-->steps.pcp.running
steps.pcp.starting-->steps.pcp.starting.started
steps.pcp.starting.started-->steps.pre_wait.starting
steps.post_wait.cancelled-->steps.post_wait.closed
steps.post_wait.cancelled-->steps.post_wait.outputs
steps.post_wait.closed-->steps.post_wait.closed.result
steps.post_wait.deploy-->steps.post_wait.closed
steps.post_wait.deploy-->steps.post_wait.starting
steps.post_wait.disabled-->steps.post_wait.disabled.output
steps.post_wait.enabling-->steps.post_wait.closed
steps.post_wait.enabling-->steps.post_wait.disabled
steps.post_wait.enabling-->steps.post_wait.enabling.resolved
steps.post_wait.enabling-->steps.post_wait.starting
steps.post_wait.outputs-->steps.pcp.cancelled
steps.post_wait.outputs-->steps.post_wait.outputs.success
steps.post_wait.running-->steps.post_wait.closed
steps.post_wait.running-->steps.post_wait.outputs
steps.post_wait.starting-->steps.post_wait.closed
steps.post_wait.starting-->steps.post_wait.running
steps.post_wait.starting-->steps.post_wait.starting.started
steps.pre_wait.cancelled-->steps.pre_wait.closed
steps.pre_wait.cancelled-->steps.pre_wait.outputs
steps.pre_wait.closed-->steps.pre_wait.closed.result
steps.pre_wait.deploy-->steps.pre_wait.closed
steps.pre_wait.deploy-->steps.pre_wait.starting
steps.pre_wait.disabled-->steps.pre_wait.disabled.output
steps.pre_wait.enabling-->steps.pre_wait.closed
steps.pre_wait.enabling-->steps.pre_wait.disabled
steps.pre_wait.enabling-->steps.pre_wait.enabling.resolved
steps.pre_wait.enabling-->steps.pre_wait.starting
steps.pre_wait.outputs-->steps.certify_all.starting
steps.pre_wait.outputs-->steps.pre_wait.outputs.success
steps.pre_wait.running-->steps.pre_wait.closed
steps.pre_wait.running-->steps.pre_wait.outputs
steps.pre_wait.starting-->steps.pre_wait.closed
steps.pre_wait.starting-->steps.pre_wait.running
steps.pre_wait.starting-->steps.pre_wait.starting.started
steps.tune_iterations.cancelled-->steps.tune_iterations.closed
steps.tune_iterations.cancelled-->steps.tune_iterations.outputs
steps.tune_iterations.closed-->steps.tune_iterations.closed.result
steps.tune_iterations.deploy-->steps.tune_iterations.closed
steps.tune_iterations.deploy-->steps.tune_iterations.starting
steps.tune_iterations.disabled-->steps.tune_iterations.disabled.output
steps.tune_iterations.enabling-->steps.tune_iterations.closed
steps.tune_iterations.enabling-->steps.tune_iterations.disabled
steps.tune_iterations.enabling-->steps.tune_iterations.enabling.resolved
steps.tune_iterations.enabling-->steps.tune_iterations.starting
steps.tune_iterations.outputs-->steps.tune_iterations.outputs.success
steps.tune_iterations.outputs.success-->steps.certify_all.starting
steps.tune_iterations.outputs.success-->steps.pcp.starting
steps.tune_iterations.running-->steps.tune_iterations.closed
steps.tune_iterations.running-->steps.tune_iterations.outputs
steps.tune_iterations.starting-->steps.tune_iterations.closed
steps.tune_iterations.starting-->steps.tune_iterations.running
steps.tune_iterations.starting-->steps.tune_iterations.starting.started
steps.uuidgen.cancelled-->steps.uuidgen.closed
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.closed-->steps.uuidgen.closed.result
steps.uuidgen.deploy-->steps.uuidgen.closed
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.disabled-->steps.uuidgen.disabled.output
steps.uuidgen.enabling-->steps.uuidgen.closed
steps.uuidgen.enabling-->steps.uuidgen.disabled
steps.uuidgen.enabling-->steps.uuidgen.enabling.resolved
steps.uuidgen.enabling-->steps.uuidgen.starting
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.uuidgen.running-->steps.uuidgen.closed
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.starting-->steps.uuidgen.closed
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.starting.started
```
