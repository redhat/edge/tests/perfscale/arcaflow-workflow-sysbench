# Automotive Sysbench + PCP Workflow

## Requirements

- [Arcaflow engine](https://github.com/arcalot/arcaflow-engine/releases) v0.9.0+
- Local container runtime, either Podman or Docker
  - *Note: Podman is configured as the default in the [`config.yaml`](config.yaml) file*

## Workflow Description

The primary [`workflow.yaml`](workflow.yaml) is designed to generate run-relevant metadata, collect system metadata with Ansible [gather facts](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/gather_facts_module.html) using the Arcaflow [metadata plugin](https://github.com/arcalot/arcaflow-plugin-metadata), and then to run loops of two sub-workflows with various parameters.

In addition to the sysbench workloads, the sub-workflows collect system metrics with [Performance Co-pilot](https://pcp.io/) using the Arcaflow [pcp plugin](https://github.com/arcalot/arcaflow-plugin-pcp). 

## Files

- [`workflow.yaml`](workflow.yaml) -- Defines the outer workflow input schema, the plugins and sub-workflow to run and their data relationships, and the output to present to the user
- [`sysbench-cpu-workflow.yaml`](sysbench-cpu-workflow.yaml) -- Defines the inner workflow input schema, plugins, and output for sysbench CPU tests. This workflow is looped over by the `workflow.yaml`, but it can also be used stand-alone.
- [`sysbench-memory-workflow.yaml`](sysbench-memory-workflow.yaml) -- Defines the inner workflow input schema, plugins, and output for sysbench memory tests. This workflow is looped over by the `workflow.yaml`, but it can also be used stand-alone.
- [`sample-input.yaml`](sample-input.yaml) -- Example input parameters that the user provides for running the outer workflow.
- [`sample-input-sysbench-cpu.yaml`](sample-input-sysbench-cpu.yaml) -- Example input parameters that can be used with the inner `sysbench-cpu-workflow.yaml` workflow stand-alone.
- [`sample-input-sysbench-memory.yaml`](sample-input-sysbench-memory.yaml) -- Example input parameters that can be used with the inner `sysbench-memory-workflow.yaml` workflow stand-alone.
- [`config.yaml`](config.yaml) -- Global config parameters that are passed to the Arcaflow
  engine
                     
## Running the Workflow

### Workflow Execution

You will need the Arcaflow engine binary v0.9.0+ and Podman or Docker to run the containers.

Download the appropriate engine binary:
https://github.com/arcalot/arcaflow-engine/releases

Clone this workflow repo, and set this directory to your workflow working directory (adjust as needed):
```
$ git clone https://gitlab.com/redhat/edge/tests/perfscale/arcaflow-workflow-sysbench.git
$ export WFPATH=$(pwd)/arcaflow-workflow-sysbench
```
 
Run the workflow (by default this will use the `workflow.yaml` file in the context directory):
```
$ arcaflow -input ${WFPATH}/input.yaml -config ${WFPATH}/config.yaml -context ${WFPATH}
```

## Workflow Diagram
This diagram shows the end-to-end workflow logic from the outer workflow perspective.

```mermaid
flowchart LR
steps.uuidgen.running-->steps.uuidgen.outputs
steps.uuidgen.running-->steps.uuidgen.crashed
steps.uuidgen.starting-->steps.uuidgen.starting.started
steps.uuidgen.starting-->steps.uuidgen.running
steps.uuidgen.starting-->steps.uuidgen.crashed
steps.system_config.deploy_failed-->steps.system_config.deploy_failed.error
steps.uuidgen.outputs-->steps.uuidgen.outputs.success
steps.uuidgen.outputs-->steps.uuidgen.outputs.error
steps.end_timestamp.outputs-->steps.end_timestamp.outputs.error
steps.end_timestamp.outputs-->steps.end_timestamp.outputs.success
steps.system_config.crashed-->steps.system_config.crashed.error
steps.end_timestamp.running-->steps.end_timestamp.outputs
steps.end_timestamp.running-->steps.end_timestamp.crashed
steps.uuidgen.crashed-->steps.uuidgen.crashed.error
steps.uuidgen.deploy_failed-->steps.uuidgen.deploy_failed.error
steps.sysbench_cpu_loop.outputs-->steps.sysbench_cpu_loop.outputs.success
steps.uuidgen.outputs.success-->outputs.success
steps.system_config.outputs.success-->outputs.success
steps.start_timestamp.cancelled-->steps.start_timestamp.outputs
steps.start_timestamp.cancelled-->steps.start_timestamp.crashed
steps.start_timestamp.cancelled-->steps.start_timestamp.deploy_failed
steps.system_config.starting-->steps.system_config.starting.started
steps.system_config.starting-->steps.system_config.running
steps.system_config.starting-->steps.system_config.crashed
steps.sysbench_memory_loop.outputs.success-->outputs.success
steps.sysbench_memory_loop.outputs.success-->steps.end_timestamp.starting
steps.system_config.cancelled-->steps.system_config.crashed
steps.system_config.cancelled-->steps.system_config.deploy_failed
steps.system_config.cancelled-->steps.system_config.outputs
steps.sysbench_cpu_loop.failed-->steps.sysbench_cpu_loop.failed.error
steps.end_timestamp.deploy-->steps.end_timestamp.starting
steps.end_timestamp.deploy-->steps.end_timestamp.deploy_failed
steps.end_timestamp.cancelled-->steps.end_timestamp.deploy_failed
steps.end_timestamp.cancelled-->steps.end_timestamp.outputs
steps.end_timestamp.cancelled-->steps.end_timestamp.crashed
steps.end_timestamp.deploy_failed-->steps.end_timestamp.deploy_failed.error
steps.sysbench_memory_loop.outputs-->steps.sysbench_memory_loop.outputs.success
steps.start_timestamp.crashed-->steps.start_timestamp.crashed.error
steps.sysbench_cpu_loop.outputs.success-->steps.sysbench_memory_loop.execute
steps.sysbench_cpu_loop.outputs.success-->outputs.success
steps.end_timestamp.crashed-->steps.end_timestamp.crashed.error
steps.system_config.outputs-->steps.system_config.outputs.success
steps.system_config.outputs-->steps.system_config.outputs.error
steps.start_timestamp.outputs-->steps.start_timestamp.outputs.error
steps.start_timestamp.outputs-->steps.start_timestamp.outputs.success
steps.uuidgen.deploy-->steps.uuidgen.starting
steps.uuidgen.deploy-->steps.uuidgen.deploy_failed
steps.sysbench_memory_loop.execute-->steps.sysbench_memory_loop.outputs
steps.sysbench_memory_loop.execute-->steps.sysbench_memory_loop.failed
steps.start_timestamp.running-->steps.start_timestamp.crashed
steps.start_timestamp.running-->steps.start_timestamp.outputs
steps.sysbench_cpu_loop.execute-->steps.sysbench_cpu_loop.outputs
steps.sysbench_cpu_loop.execute-->steps.sysbench_cpu_loop.failed
steps.start_timestamp.deploy_failed-->steps.start_timestamp.deploy_failed.error
steps.uuidgen.cancelled-->steps.uuidgen.outputs
steps.uuidgen.cancelled-->steps.uuidgen.crashed
steps.uuidgen.cancelled-->steps.uuidgen.deploy_failed
steps.start_timestamp.starting-->steps.start_timestamp.starting.started
steps.start_timestamp.starting-->steps.start_timestamp.running
steps.start_timestamp.starting-->steps.start_timestamp.crashed
steps.sysbench_memory_loop.failed-->steps.sysbench_memory_loop.failed.error
steps.end_timestamp.outputs.success-->outputs.success
input-->steps.sysbench_cpu_loop.execute
input-->steps.sysbench_memory_loop.execute
input-->outputs.success
steps.start_timestamp.outputs.success-->outputs.success
steps.start_timestamp.outputs.success-->steps.sysbench_cpu_loop.execute
steps.system_config.running-->steps.system_config.outputs
steps.system_config.running-->steps.system_config.crashed
steps.system_config.deploy-->steps.system_config.starting
steps.system_config.deploy-->steps.system_config.deploy_failed
steps.start_timestamp.deploy-->steps.start_timestamp.starting
steps.start_timestamp.deploy-->steps.start_timestamp.deploy_failed
steps.end_timestamp.starting-->steps.end_timestamp.starting.started
steps.end_timestamp.starting-->steps.end_timestamp.running
steps.end_timestamp.starting-->steps.end_timestamp.crashed

```
